#!/bin/sh


main() {
    mkdir -p ~/.scripts/cf_ips
    
    echo "Setting up cf ip whitelister into ~/.scripts/cf_ips"
    echo '
#!/bin/sh
PATH=$PATH:/sbin

wget https://www.cloudflare.com/ips-v4 -qO /tmp/cf_ips
echo "" >> /tmp/cf_ips
wget https://www.cloudflare.com/ips-v6 -qO ->> /tmp/cf_ips

for cfip in `cat /tmp/cf_ips`; do ufw allow proto tcp from $cfip to any port 80,443 comment "Cloudflare IP"; done

rm /tmp/cf_ips
    ' > ~/.scripts/cf_ips/cf_ips.sh
    
    chmod +x ~/.scripts/cf_ips/cf_ips.sh
    
    echo "Setting up cron job to run every hour"
    echo "0 0 * * * ~/.scripts/cf_ips/cf_ips.sh" | crontab -
    
    echo "Executing Running cf ip whitelister for the first time"
    ~/.scripts/cf_ips/cf_ips.sh
    
    echo "Done"
    echo "To enable the firewall run: sudo ufw enable"
    echo "(!) Make sure to allow ssh access to your server first."
}

# Run main
main
